<!DOCTYPE html>
<html>
<!-- Esto es un comentario. La etiqueta html da inicio y formato al archivo del sitio web -->

	<head>
		<!-- La etiqueta head es el encabezado de la pagina, guarda los enlaces a CSS, JS y los script -->
		
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<!-- De esta manera cargamos los CSS -->
		
        <link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/bootstrap-select.css" />
		
		<script language="JavaScript" src="jquery.min.js"></script>
		<!-- De esta manera cargamos los JS - librerias de programacion -->
		
		<script language="JavaScript" src="js/bootstrap.min.js"></script>
		<script language="JavaScript" src="js/bootstrap-select.min.js"></script>
		
		<script>
			<!-- Dentro de script colocamos las funciones y programas en javascript -->
			
			function listagrabaciones(){
				var dataString = 'anolanzamiento='+$('#anolanzamientobuscar').val();
										
				$.ajax({
					type: "POST",
					url:"./listagrabaciones.php",
					data: dataString,
					success: function(data){
						$("#resultado").html(data);
						$("#resultado").show();
						}
				});
			}
			
			function listainterpretes(){
				var dataString = 'nombre='+$('#nombrebuscar').val();
										
				$.ajax({
					type: "POST",
					url:"./listainterpretes.php",
					data: dataString,
					success: function(data){
						$("#resultado").html(data);
						$("#resultado").show();
						}
				});
			}
			
			function guardainterpretes(){
				var dataString = 'nombre='+$('#nombre').val()+
								 '&nacionalidad='+$('#nacionalidad').val();
				$.ajax({
					type: "POST",
					url:"./guardainterpretes.php",
					data: dataString,
					success: function(data){
						
						$("#resultado").html(data);
						$("#resultado").show();
						}
				});
			}
			
			function guardaalbum(){
				var dataString = 'titulo='+$('#titulo').val()+
								 '&interprete='+$('#interprete').val()+
								 '&genero='+$('#genero').val()+
								 '&anolanzamiento='+$('#anolanzamiento').val()+
								 '&disponible='+$('#disponible').val();
				console.log(dataString);
				$.ajax({
					type: "POST",
					url:"./guardaalbum.php",
					data: dataString,
					success: function(data){
						
						$("#resultado").html(data);
						$("#resultado").show();
						}
				});
			}
		
		$(document).ready(function(){
			
			$("#btn4").click(function(event){
				$("#resultado").load('./interpreteform.php');
			});
			
			$("#btn3").click(function(event){
				$("#resultado").load('./albumform.php');
			});
			
		});
		
	</script>
	</head>
	
	<body>
		<!-- La etiqueta body es el cuerpo de la pagina. La parte que sera visible para el usuario -->
		
		<div id="menu" align="center">
			<!-- La etiqueta div nos permite separar el contenido en partes con las cuales podemos interactuar con programacion -->
			
			<table width="50%">
			<!-- La etiqueta table nos permite insertar una tabla en la pagina, para organizar el contenido -->
			
				<tr>
					<!-- La etiqueta tr crea una fila o renglon en la tabla -->
					
					<td>
						<!-- La etiqueta td crea una celda en la fila. Lugar en el cual ubicamos el contenido -->
						
						<button class="boton" id="btn1" onclick="listagrabaciones()">Listado Grabaciones</button>
						<!-- De esta manera creamos un boton que ejecuta la funcion listagrabaciones() al cliquear -->
					</td>
					<td>
						<p> Año: </p>
						<!-- Las etiquetas P nos permiten incorporar texto a la pagina -->
					</td>
					<td>
						<input type="text" class="form-control" name="anolanzamiento" id="anolanzamientobuscar"/>
						<!-- De esta manera incorporamos un campo en la pagina para que el usuario pueda ingresar informacion de tipo texto-->
						
					</td>
					<td>
						<button class="boton" id="btn3" onclick=""> Nuevo Album</button>
					</td>
				</tr>
				<tr>
					<td>
						<button class="boton" id="btn2" onclick="listainterpretes()">Listado Interpretes</button>
					</td>
					<td>
						<p> Nombre: </p>
					</td>
					<td>
						<input type="text" class="form-control" name="nombrebuscar" id="nombrebuscar"/>
					</td>
					<td>
						<button class="boton" id="btn4" onclick="forminterprete()"> Nuevo Interprete</button>
					</td>
				</tr>
				<tr><td><br></td></tr>
			</table>
		</div>
		<div id="resultado" align="center">
		<!-- En el div resultado, cargamos las respuestas del sistema al interactuar el usuario -->
		
		</div>
	
	
	</body>
	</html>
<?php
	
?>
