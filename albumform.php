<script>
		$(document).ready(function(){
			var select = document.getElementById('selinterprete');
			select.addEventListener('change', function(){
			var selectedOption = this.options[select.selectedIndex];
			console.log(selectedOption.value + ': ' + selectedOption.text);
			document.getElementById("interprete").value = selectedOption.value;
			});
		});
</script>
<?php
require 'conexion.php';
//La funcion require permite incorporar el texto del archivo conexion.php al inicio de este archivo, para poder realizar la conexion a la base de datos.

	try{
				$conexion = new PDO($dsn, "SET NAMES 'UTF-8'");
				//Se crea una nueva conexion.
			}catch (PDOException $e){
					echo 'Error: ' . $e->getMessage();
					//Si se genera un error, se muestra al usuario.
			}

			$sqlnac = "SELECT * FROM interpretes";
		
			$listanac = $conexion->query($sqlnac);
			//Se realiza la consulta solicitada.
			
			$arraynac = array();
			//Inicializamos un array para poder guardar los datos que devuelve la base de datos.
			
			foreach($listanac as $nac){
				//Para cada resultado obtenido, se ingresan los datos en el array.
				
				array_push($arraynac, $nac);
				
			}
			//var_dump($arraynac);
			//echo $sqlnac;
?>

<form action="./guardaalbum.php" method="POST" >
<!-- Asi creamos un formulario, la accion es el archivo al cual enviamos los datos ingresados. Por metodo POST -->
	<table>
		<tr>
			<td><h3>Nuevo Album:</h3></td>
		</tr>
		<tr>
			<td>
				<p>Titulo: </p>
				<input type="text" class="form-control" name="titulo" id="titulo"/>
			</td>
		</tr>
		<tr>
			<td>
				<p>Interprete: </p>
				<input type="text" class="form-control" name="interprete" id="interprete"/>
				
				<select name="selinterprete" id="selinterprete" class="form-control" > 
                        <option value="0">Seleccione un interprete.</option>
                        <?php
                        foreach ($arraynac as $fila){ 
							$indice = $fila['IDInterprete'];
							$valor = $fila['Nombreinterprete'];
                        ?>
							<option value="<?=$indice;?>"><?=$valor;?></option> 
                        <?php
                        }
                        ?>
                    </select>
			</td>
		</tr>
		<tr>
			<td>
				<p>Genero: </p>
				<input type="text" class="form-control" name="genero" id="genero"/>
			</td>
		</tr>
		<tr>
			<td>
				<p>Año Lanzamiento: </p>
				<input type="text" class="form-control" name="anolanzamiento" id="anolanzamiento"/>
			</td>
		</tr>
		<tr>
			<td>
				<p>Disponible: </p>
				<input type="checkbox" class="form-control" name="disponible" id="disponible"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<button type="button" value="Guardar" onclick="guardaalbum()">Guardar</button>
				<!-- Al cliquear el boton los datos ingresados en los campos pasan al servidor por medio de AJAX -->
				
			</td>
		</tr>
	</table>
</form>